import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Code Reviews</h1>

    <p>A code review is when one or more developers reads, and verifies antoher developers code. 
      Code reviews have many benefits for Software Engineering, especially at scale (google).

      They catch bugs, reduce technical debt, promote uniform code standards, and improves knowledge sharing.
    </p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link>
  </Layout>
)

export default IndexPage
